﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class interactController : MonoBehaviour
{
    
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean trackpadClickAction;
    public SteamVR_Action_Boolean grabAction;
    public SteamVR_Action_Boolean triggerAction;
    public SteamVR_Action_Vibration vibration;
    public SteamVR_Action_Vector2 trackpadAction;

    private bool holding;
    private GameObject heldObject;
    private Interactable heldInteractible;

    private GameObject collidingObject;

    private SteamVR_RenderModel renderModel;

    private FixedJoint createFixedJoint()
    {
        FixedJoint joint = gameObject.AddComponent<FixedJoint>();
        joint.breakForce = Mathf.Infinity;
        joint.breakTorque = Mathf.Infinity;
        return joint;
    }

    public void lockToHand()
    {
		highlightCollidingObject(false);
        setObjectPosition(collidingObject);
        heldObject = collidingObject;
        heldInteractible = heldObject.GetComponent<Interactable>();
        collidingObject = null;

        renderModel.SetMeshRendererState(false);

        FixedJoint joint = createFixedJoint();
        joint.connectedBody = heldObject.GetComponent<Rigidbody>();
		joint.connectedBody.useGravity = false;
		
    }
    public void unlockFromHand()
    {
        if (GetComponent<FixedJoint>())
        {
            FixedJoint joint = GetComponent<FixedJoint>();
            joint.connectedBody = null;
            Destroy(joint);
        }

        renderModel.SetMeshRendererState(true);
		heldObject.GetComponent<Rigidbody>().useGravity = true;
        heldObject = null;
    }

    private void highlightCollidingObject(bool highlight = true) {
        if (!heldObject)
        {
            if (collidingObject.GetComponent<Interactable>())
            {
                Interactable interactable = collidingObject.GetComponent<Interactable>();
                interactable.SetHighlight(highlight);
            }
        }
    }

    private void setObjectPosition(GameObject o) {
        o.transform.position = transform.position;
        o.transform.rotation = transform.rotation;
    }

    
    private void setCollidingObject(Collider col) {
        if (collidingObject || !col.GetComponent<Rigidbody>()) {
            return;
        }
        
        collidingObject = col.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
		if(other.gameObject.GetComponent<Interactable>()) {
            setCollidingObject(other);
			highlightCollidingObject();
		}
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.GetComponent<Interactable>()) {
            setCollidingObject(other);
			highlightCollidingObject();
		}
    }
    private void OnTriggerExit(Collider other)
    {
        if (!collidingObject) {
            return;
        }
        highlightCollidingObject(false);
        collidingObject = null;
    }

    private void Start()
    {
        renderModel = GetComponentInChildren<SteamVR_RenderModel>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (triggerAction.GetLastStateDown(handType)){
            if (collidingObject && !heldObject) {
                lockToHand();
            }
            else if (heldObject) {
                unlockFromHand();
            }
        }
        
        if (grabAction.GetLastStateDown(handType)) {
            if (heldObject) {
                
                heldInteractible.GrabAction();

            }
        }
    }
}
