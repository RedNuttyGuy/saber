﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public int team;

    public float maxHealth;

    private float health {
        set
        {
            if (Alive)
            {
                health = (health > maxHealth) ? Health + value : maxHealth;
            }
        }
        get { return health; }
    }
    public float Health {
        get { return health; }
    }
    private float wantedHealth;

    public bool Alive;

    private List<float[]> HoTSources = new List<float[]>();
    private List<float[]> DoTSources = new List<float[]>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // apply over time effects
        if (HoTSources.Count != 0) {
            for (int i = 0; i < HoTSources.Count; i++)
            {
                float amount = HoTSources[i][0];
                float duration = HoTSources[i][1];

                HoTSources[i][1] -= Time.deltaTime;

                float healIteration;

                if (HoTSources[i][1] <= 0)
                {
                    healIteration = amount * duration;
                    HoTSources.RemoveAt(i);
                }
                else
                {
                    healIteration = amount * Time.deltaTime;
                }

                Heal(healIteration);
            }
        }

        if (DoTSources.Count != 0) {
            for (int i = 0; i < DoTSources.Count; i++)
            {
                float amount = DoTSources[i][0];
                float duration = DoTSources[i][1];

                HoTSources[i][1] -= Time.deltaTime;

                float damageIteration;

                if (HoTSources[i][1] <= 0)
                {
                    damageIteration = amount * duration;
                    HoTSources.RemoveAt(i);
                }
                else
                {
                    damageIteration = amount * Time.deltaTime;
                }

                Damage(damageIteration);
            }
        }
    }


    public void Kill()
    {
        Alive = false;
        health = 0;
    }

    public void Heal(float amount)
    {
        health += amount;
    }
    public void HealOverTime(float perSecond, float duration)
    {
        HoTSources.Add(new float[] { perSecond, duration });
    }
    public void Damage(float amount)
    {
        health -= amount;
        if (Health <= 0)
        {
            Kill();
        }
    }
    public void DamageOverTime(float perSecond, float duration)
    {
        DoTSources.Add(new float[] { perSecond, duration });
    }



}
