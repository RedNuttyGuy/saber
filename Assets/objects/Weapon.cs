﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public int team;
    public float Damage;

    public bool Projectile;
    public float ProjectileSpeed;
    public bool useGravity;
    public float minVelocity = 0;
    private Vector3 velocity;

    private Rigidbody rb;
    private Collider col;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Rigidbody>()) {
            rb = GetComponent<Rigidbody>();
        }
        if (GetComponent<Collider>()) {
            col = GetComponent<Collider>();
        }

        if (Projectile) {
            rb.isKinematic = true;
            velocity = transform.rotation * Vector3.forward * ProjectileSpeed;
        }
    }

    void Update()
    {

        if (Projectile) {

            if (useGravity) {
                velocity += Physics.gravity;
            }

            if (velocity.magnitude > minVelocity) {
                Vector3 newpos = transform.position + velocity;
                RaycastHit hit;

                if (Physics.Linecast(transform.position, newpos, out hit)) {
                    if (hit.transform.gameObject.tag == "Reflective")
                    {
                        velocity = Vector3.Reflect(velocity, hit.normal);
                    }
                    else if (hit.transform.gameObject.GetComponent<Character>())
                    {
                        Character c = hit.transform.gameObject.GetComponent<Character>();
                        if (c.team != team)
                        {
                            c.Damage(Damage);
                            Destroy(gameObject);
                        }
                    }
                    else {
                        Destroy(gameObject);
                    }
                }
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!Projectile)
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                float pointVelocity = rb.GetPointVelocity(contact.point).magnitude;
                if (pointVelocity > minVelocity)
                {
                    if (contact.otherCollider.gameObject.GetComponent<Character>())
                    {
                        contact.otherCollider.gameObject.GetComponent<Character>().Damage(Damage * pointVelocity);
                    }
                }
            }
        }
    }
}
