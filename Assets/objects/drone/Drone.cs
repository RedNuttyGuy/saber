﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : MonoBehaviour
{
    public float MaxDistanceFromPlayer = 3;
    public float MoveDistance = 5;
    public float Speed;
    private float acceleration;
    public float Acceleration
    {
        get { return acceleration; }
        set { acceleration = (value < 0) ? 0 : value; }
    }

    public GameObject debrisObject;

    public GameObject Target;
    private Vector3 targetOffset;
    private Vector3 wantedPosition;
    public float wantedPositionTime;
    private float wantedPositionTimeout = 0;

    private Character character;
    private Rigidbody rb;

    private bool hit;
    private float hitTimeout;

    void Start()
    {

        character = GetComponent<Character>();
        rb = GetComponent<Rigidbody>();
        Target = Camera.main.gameObject;
    }

    void Update()
    {
        if (!character.Alive)
        {
            deathEffect();
        }
        else
        {

            if (hitTimeout > 0)
            {
                hitTimeout -= Time.deltaTime;
            }
            else if (wantedPositionTimeout > 0)
            {
                wantedPositionTimeout -= Time.deltaTime;

                Quaternion lookRotation = Quaternion.LookRotation(Target.transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, 0.05f);
            }
            else
            {
                wantedPositionTimeout = wantedPositionTime;
                wantedPosition = Random.insideUnitSphere * MaxDistanceFromPlayer;
                wantedPosition.y = Mathf.Abs(wantedPosition.y);
                wantedPosition += Target.transform.position;

            }

        }
        
        
        

    }

    private void FixedUpdate()
    {
        if (wantedPositionTimeout > 0) {
            float s = Mathf.Min(Vector3.Distance(transform.position, wantedPosition), Speed);
            rb.velocity = (wantedPosition - transform.position).normalized * s;
            rb.angularVelocity = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Hit();
    }

    private void deathEffect()
    {
        GameObject debris = Instantiate(debrisObject, transform.position, transform.rotation);

        Rigidbody[] debrisrb = debris.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody child in debrisrb) {
            child.velocity += Random.insideUnitSphere * 2;
        }

        Destroy(gameObject);
    }

    public void Hit() {
        hitTimeout = 2.5f;
    }
}
