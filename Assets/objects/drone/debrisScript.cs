﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debrisScript : MonoBehaviour
{
    private bool timerStarted = false;
    public float timer = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent.childCount <= 1)
        {
            Destroy(transform.parent.gameObject);
        }
        if (timerStarted) {
            if (timer <= 0) {
                Destroy(gameObject);
            }
            timer -= Time.deltaTime;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        timerStarted = true;
    }
}
