﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaberAudioController : MonoBehaviour
{

    private float pitch;
    private float pitchMod;
    private Vector3 oldPosition;
    public float pitchAdjust;
    private AudioSource audioSource;
    private float startingPitch;
    private float volume;

    // Start is called before the first frame update
    void Start()
    {
        oldPosition = transform.position;

        audioSource = GetComponent<AudioSource>();
        startingPitch = audioSource.pitch;
    }

    // Update is called once per frame
    void Update()
    {

        pitchMod = 10 * Mathf.Log10((oldPosition - transform.position).magnitude + 1) + 1;
        volume = 
        pitch = startingPitch + (pitchMod * pitchAdjust);

        audioSource.pitch = pitch;
        oldPosition = transform.position;



    }
}
