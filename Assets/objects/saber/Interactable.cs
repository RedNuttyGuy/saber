﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR;
using Valve.VR.InteractionSystem;

public class Interactable : MonoBehaviour
{
    private Material highlightMat;
    private GameObject highlightObject;

    public delegate void triggerAction();
    public delegate void grabAction();

    public triggerAction TriggerAction;
    public grabAction GrabAction;

    public void Start()
    {
        createHighlight();
    }

    private void createHighlight() {
        highlightMat = (Material)Resources.Load("SteamVR_HoverHighlight", typeof(Material));

        highlightObject = new GameObject("Highlight");
        highlightObject.transform.parent = gameObject.transform;
        highlightObject.transform.localPosition = Vector3.zero;

        MeshRenderer meshRenderer = highlightObject.AddComponent<MeshRenderer>();
        MeshFilter meshFilter = highlightObject.AddComponent<MeshFilter>();

        meshFilter.mesh = gameObject.GetComponent<MeshFilter>().mesh;
        meshRenderer.material = highlightMat;

        SetHighlight(false);
    }

    public void SetHighlight(bool setHighlight = true) {
        if (setHighlight)
        {
            highlightObject.SetActive(true);
        }
        else
        {
            highlightObject.SetActive(false);
        }
    }

    public void moveToPosition(Vector3 position, Quaternion rotation) {
        transform.position = position;
        transform.rotation = rotation;
    }



}
