﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaberBlade : MonoBehaviour
{

    public GameObject collisionEffect;
    private List<GameObject> collisionParticles = new List<GameObject>();


    // Start is called before the first frame update
    void Start()
    {

        //Physics.IgnoreCollision(GetComponent<Collider>(), gameObject.GetComponentInParent<Collider>(), true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log(collision.contacts[0].point);
        //ParticleSystem p = Instantiate(collisionEffect);
        //p.transform.parent = gameObject.transform.parent.transform;
        //collisionParticles.Add(p);
        
    }

    private void OnCollisionStay(Collision collision)
    {
        while (collisionParticles.Count < collision.contactCount)
        {
            GameObject p = Instantiate(collisionEffect);
            collisionParticles.Add(p);
        }
        while (collisionParticles.Count > collision.contactCount)
        {
            GameObject p = collisionParticles[0];
            collisionParticles.Remove(p);
            Destroy(p);
            
        }
        
        for (int i = 0; i < collision.contactCount; i++) {
            collisionParticles[i].transform.position = collision.GetContact(i).point;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        /*
        GameObject p = collisionParticles[0];
        collisionParticles.RemoveAt(0);
        Destroy(p);
        */
    }
}
