﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saber : MonoBehaviour
{

    private bool extended = false;

    public ParticleSystem bladeCollisionParticle;

    private Interactable interactable;

    private GameObject blade;
    private Renderer rend;
    public Color userColor;
    private Color colorPad;
    private Color bladeColor;
    private Animator anim;

    private GameObject audio;
    private AudioSource saberAudio;
    private AudioSource extendAudio;

    public AudioClip ExtendSound;
    public AudioClip CollapseSound;

    private Light[] lights;

    // Start is called before the first frame update
    void Start()
    {
        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (int i = 0; i < childColliders.Length; i++) {
            Physics.IgnoreCollision(childColliders[i], GetComponent<Collider>(), true);
        }
        

        interactable = GetComponent<Interactable>();
        anim = GetComponent<Animator>();
        blade = gameObject.transform.Find("blade").gameObject;
        
        rend = blade.GetComponent<Renderer>();
        extendAudio = blade.GetComponent<AudioSource>();

        audio = gameObject.transform.Find("audio").gameObject;
        saberAudio = audio.GetComponent<AudioSource>();
        
        colorPad = new Color(0.1f, 0.1f, 0.1f);

        lights = GetComponentsInChildren<Light>();

        interactable.GrabAction = cycleBlade;
    }

    // Update is called once per frame
    void Update()
    {

        GetComponent<Rigidbody>().centerOfMass = Vector3.zero;
        if (extended)
        {
            randomizeBladeEmission();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            cycleBlade();
        }

    }

    private void randomizeBladeEmission() {
        float rand = Random.Range(0.9f, 1.1f);
        bladeColor = (userColor * rand) + colorPad;
        bladeColor *= 10 / userColor.maxColorComponent;
        rend.material.SetColor("_EmissionColor", bladeColor);

        for (int i = 0; i < lights.Length; i++) {
            lights[i].range = rand;
        }
    }

    private void manipulateAudio() {
        
    }

    private void changeBladeExtension(bool extension) {
        if (extension)
        {
            // expand blade
            anim.Play("saberExtend", 0);
            extendAudio.clip = ExtendSound;
            extendAudio.Play();
            extended = true;
        }
        else
        {
            // collapse blade
            anim.Play("saberCollapse", 0);
            extendAudio.clip = CollapseSound;
            extendAudio.Play();
            extended = false;
        }
    }

    public void collapseBlade() {
        changeBladeExtension(false);
    }
    public void extendBlade()
    {
        changeBladeExtension(true);
    }

    private void cycleBlade() {
        if (extended)
        {
            collapseBlade();
        }
        else
        {
            extendBlade();
        }
    }






}
